<?php

/**
 * @file
 * callbacks for ecard resource
 */

/**
 * Implementation callback for ecard.retrieve
 *
 * @param $hash string
 *   the hash identifying this ecard entity
 * @return Entity
 *   the retrieved Entity instance
 *
 */
function _ecard_resource_retrieve($hash) {
  return array_shift(entity_load('ecard', array($hash)));
}

/**
 * Implementation callback for ecard.create
 *
 * @throws Exception
 *   if something goes wrong within data processing
 * @param $ecard stdClass|array
 *   object or keyed array with ecard entity properties
 * @return string
 *   the hash of newly created ecard entity upon success or
 *   FALSE in case of failure
 */
function _ecard_resource_create($ecard) {
  if (!is_array($ecard)) {
    $ecard = (array)$ecard;
  }


  $entity = array_shift(entity_load($ecard['entity_type'], array($ecard['entity_id'])));

  $ids = entity_extract_ids($ecard['entity_type'], $entity);

  $ecard['bundle'] = $ids[2];

  foreach (field_info_instances($ecard['entity_type'], 'ecard') as $instance) {

    $field = field_info_field($instance['field_name']);
    if ($field['type'] == 'ecard') {

      $args = ecard_get_args($ecard['entity_type'], $entity, $field, $instance);

      $args['mail'] = array(
        'letter' => $instance['settings']['letter'],
        'copy' => $instance['settings']['copy'],
      );


      break;

    }

  }

  if (!isset($args)) {
    throw new Exception("cannot find ecard args");
  }


  $ecard['uid'] = $GLOBALS['user']->uid;

  // Remove any HTML and other unwanted chars that might destroy the senders name.
  $ecard['name_from'] = str_replace(array(',', '<', '>'), '', check_plain($ecard['name_from']));
  //$ecard['mail_from'] = $form['mail_from']['#value'];

  $ecard['name_to'] = str_replace(array(',', '<', '>'), '', check_plain($ecard['name_to']));
  // $ecard['mail_to'] = $form['mail_to']['#value'];

  // Make sure nothing bad can happen.
  // @todo Implement input filter system.
  $ecard['text'] = filter_xss_admin($ecard['text']);


  $ecard = ecard_create($ecard);

  $ecard->entity_path = $args['entity_path'];
  $ecard->mail = $args['mail'];
  $ecard->ecard_url = url($ecard->entity_path, array('query' => array('ecard' => $ecard->hash), 'absolute' => TRUE));
  $ecard->site_name = variable_get('site_name', 'Default site name');


  ecard_mail_send($ecard);

  return $ecard->hash;

}

