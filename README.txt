Services for Ecard

by Romain Poye (romain@icilalune.com)
by Simon Morvan (garphy@zone84.net)

work sponsored by ICI LA LUNE (http://www.icilalune.com/)

This module expose a Services 3.x API resource for Ecard entities.
(see http://drupal.org/project/ecard).

It aims to serve as an entry point for external ecard viewers & producer
to use a base ecard CRUD API.
